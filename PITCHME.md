# HeedView

![HeedViewLogo](assets/img/HeedView-icon-512.png =250x)

4. oktoober 2019

---

#### Kas kõigil on olemas konto? Kas kõik mäletavad oma salasõna?

---

## Uus mobiili äpp ja *desktop* rakendus

* Nüüdsest on võimalik HeedView telefoni alla laadida, nagu korralik äpp.
    * Androidiga on lihtne, iPhonega peab vist natuke pusima.
* Rakenduse saab alla laadida ka arvutisse, töölauale tekib ilus nupuke ning programm avaneb eralid aknas.
* Midagi sisulist see ei muuda, aga kasutada on mugavam.

---

#### Kiire tutvustus: kasutamine ja võimalused

---

## Ettepanekud

Kõik mured, takistused,
soovitused ja tulevikuplaanid :)
